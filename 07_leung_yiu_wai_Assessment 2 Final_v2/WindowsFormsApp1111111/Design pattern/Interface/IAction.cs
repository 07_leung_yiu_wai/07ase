﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assessment1.New.Interface
{
    public interface IDrawAction
    {
        /// <summary>
        /// moveto current x y to tx.ty
        /// </summary>
        /// <param name="tx"></param>
        /// <param name="ty"></param>
        void Draw(int tx,int ty);
        /// <summary>
        /// draw point
        /// </summary>
        /// <param name="points"></param>
        void Draw(Point[] points);
        /// <summary>
        /// moveto target tx ty and re-define x y
        /// </summary>
        /// <param name="tx"></param>
        /// <param name="ty"></param>
        void MoveTo(int tx,int ty);

        void SetPen(Pen pen);
        void SetFill(String fill);

        void Reset();
    }
    public interface ICommandAction
    {
        /// <summary>
        /// Execute CMD
        /// </summary>
        string[] Run(System.Windows.Forms.OpenFileDialog openFileDialog1);
        /// <summary>
        /// Clear CMD 
        /// </summary>
        void Clear();

        void Load(System.Windows.Forms.OpenFileDialog openFileDialog1);

        void Save(System.Windows.Forms.SaveFileDialog saveFileDialog1);



    }

    public interface IFileAction
    {
        /// <summary>
        /// Load
        /// </summary>
        /// <param name="filepath"></param>
        void Load(string filepath);
        /// <summary>
        /// Save
        /// </summary>
        /// <param name="filepath"></param>
        void Save(string filepath);
    }
}
