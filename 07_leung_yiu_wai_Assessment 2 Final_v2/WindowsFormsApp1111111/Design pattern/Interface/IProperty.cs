﻿using Assessment1.New.Enum;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assessment1.New.Interface
{
    public interface IProperty
    {
        ESharpType sharpType { get; set; }
        Pen pen { get; set; }
        Graphics graphics { get; set; }
        int curX { get; set; }
        int curY { get; set; }

        string fill { get; set; }


        Pen erasePos { get; set; }
        Pen myPen { get; set; }
        Pen myPos { get; set; }

    }
}
