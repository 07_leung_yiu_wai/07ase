﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assessment1.New.Enum
{
    public enum ESharpType
    {
        Point=0,
        Line,
        Rectangle,
        Circle,
        Triangle,
        Reset
    }
}
