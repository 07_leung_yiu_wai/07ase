﻿using Assessment1.New.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Assessment1.New.Factory
{
    public class CommandFactory : ICommandAction
    {
        List<string> Commands = new List<string>();
        public void Clear()
        {

        }

        public void Load(System.Windows.Forms.OpenFileDialog openFileDialog1)
        {
            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Text File(txt)|*.txt";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                String sr_filePath;
                sr_filePath = openFileDialog1.FileName;
                //Read the contents of the file into a stream

                Commands.Clear();
                   var sr_fileContent = System.IO.File.ReadLines(sr_filePath);
                foreach (var item in sr_fileContent)
                {
                    if (!string.IsNullOrEmpty(item))
                        Commands.Add(item);
                }
                    
                
            }

        }

        public string[] Run(System.Windows.Forms.OpenFileDialog openFileDialog1)
        {
            Load(openFileDialog1);
            return Commands.ToArray();
        }

        public void Save(System.Windows.Forms.SaveFileDialog saveFileDialog1)
        {

            saveFileDialog1.Filter = "Text File(txt)|.txt";
            saveFileDialog1.FileName = String.Empty;
            saveFileDialog1.DefaultExt = ".txt";
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                
                System.IO.File.WriteAllLines(saveFileDialog1.FileName, Commands);

            }

        }
        
    }
}
