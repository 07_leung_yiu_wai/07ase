﻿using Assessment1.New.Enum;
using Assessment1.New.Interface;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Assessment1.New.Factory
{
    public class SharpFactory :IDrawAction, IProperty
    {
        private IDrawAction drawAction;
        
        public SharpFactory(Graphics g,ESharpType eSharp)
        {
            graphics = g;
            sharpType = eSharp;
            switch (eSharp)
            {
                case ESharpType.Line:
                    {
                        var line= new Cls.MyLine(g, eSharp); ;
                        drawAction = line;
                        break;
                    }
                case ESharpType.Rectangle:
                    {
                        var rectangle = new Cls.MyRectangle(g, eSharp); ;
                        drawAction = rectangle;
                        break;
                    }
                case ESharpType.Circle:
                    {
                        var circle= new Cls.MyCircle(g, eSharp); ;
                        drawAction = circle;
                        break;
                    }
                case ESharpType.Triangle:
                    {
                        var triangle = new Cls.MyTriangle(g, eSharp); ;
                        drawAction = triangle;
                        break;
                    }
            }

            drawAction.SetPen(new Pen(Color.Black));
        }


        public ESharpType sharpType {get;set;}
        public Pen pen {get;set;}
        public Graphics graphics {get;set;}
        public int curX {get;set;}
        public int curY {get;set;}
        public string fill {get;set;}
        public Pen erasePos { get; set; }= new Pen(Color.LightGray, 2);
        public Pen myPen { get; set; } = new Pen(Color.Black, 2);
        public Pen myPos { get; set; } = new Pen(Color.Red, 1);

       /// <summary>
       /// Constructores
       /// </summary>
       /// <param name="tx"></param>
       /// <param name="ty"></param>
        public void Draw(int tx, int ty)
        {
            drawAction.Draw(tx, ty);
        }

        public void Draw(Point[] points)
        {
            drawAction.Draw(points);
        }

        public void MoveTo(int tx, int ty)
        {
            drawAction.MoveTo(tx, ty);
        }

        public void Reset()
        {
            
        }

        public void SetFill(string fill)
        {
            drawAction.SetFill(fill);
        }

        public void SetPen(Pen pen)
        {
            drawAction.SetPen( pen);
        }
    }
}
