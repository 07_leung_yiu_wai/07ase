﻿using Assessment1.New.Enum;
using Assessment1.New.Interface;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assessment1.New.Cls
{
   public class MyTriangle : IDrawAction, IProperty
    {

        public MyTriangle(Graphics g, ESharpType eSharp)
        {
            graphics = g;
            sharpType = ESharpType.Triangle;
        }

        public ESharpType sharpType { get; set; }
        public Pen pen { get; set; }
        public Graphics graphics { get; set; }
        public int curX { get; set; }
        public int curY { get; set; }
        public string fill { get; set; } = "off";
        public Pen erasePos { get; set; } = new Pen(Color.LightGray, 2);
        public Pen myPen { get; set; } = new Pen(Color.Black, 2);
        public Pen myPos { get; set; } = new Pen(Color.Red, 1);

        public void Draw(int tx, int ty)
        {
            curX = tx;
            curY = ty;
        }

        public void Draw(Point[] points)
        {

            if (!string.IsNullOrEmpty(fill) && fill == "on")
            {
                SolidBrush blueBrush = new SolidBrush(Color.Blue);
                graphics.FillPolygon(blueBrush, points);
            }
            else
            {
                graphics.DrawPolygon(pen, points);
            }
          

            curX = points.Last().X;
            curY = points.Last().Y;
        }

        public void MoveTo(int tx, int ty)
        {
            curX = tx;
            curY = ty;
        }

        public void SetFill(string fill)
        {
            this.fill = string.IsNullOrEmpty(fill) || fill != "on" ? "off" : "on";
        }

        public void SetPen(Pen pen)
        {
            this.pen = pen;
        }
        public void Reset()
        {
            graphics.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
            graphics.DrawEllipse(erasePos, curX - 7, curY - 7, 14, 14);
            graphics.DrawEllipse(erasePos, curX - 9, curY - 9, 18, 18);
            graphics.DrawLine(erasePos, curX - 15, curY, curX + 15, curY);
            graphics.DrawLine(erasePos, curX, curY - 15, curX, curY + 15);
            graphics.DrawLine(myPen, new Point(0, 0), new Point(0, 0));
            curX = 0;
            curY = 0;
            graphics.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
            graphics.DrawEllipse(myPos, curX - 7, curY - 7, 14, 14);
            graphics.DrawEllipse(myPos, curX - 9, curY - 9, 18, 18);
            graphics.DrawLine(myPos, curX - 15, curY, curX + 15, curY);
            graphics.DrawLine(myPos, curX, curY - 15, curX, curY + 15);
        }
    }
}
