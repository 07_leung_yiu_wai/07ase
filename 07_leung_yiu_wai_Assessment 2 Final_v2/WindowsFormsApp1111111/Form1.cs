﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Windows.Forms.VisualStyles;
using Parser.Library;
using Assessment1.New.Cls;
using Assessment1.New.Factory;
using System.Collections;

namespace Assessment1
{
    public partial class Form1 : Form
    {   
        static int curX = 0, curY = 0, param1 = 0, param2 = 0, param3 = 0, param4 = 0, param5 = 0, param6 = 0;
        static Pen myPen = new Pen(Color.Black, 2);
        /// <summary>
        /// Default pen color
        /// </summary>
        static Pen myPos = new Pen(Color.Red, 1);
        static Pen erasePos = new Pen(Color.LightGray, 2);
        static CommandParser myParser = new CommandParser();
        /// <summary>
        /// Library to commandparser
        /// </summary>
        static Point origin1 = new Point(0, 0);      //origin point x 
        static Point origin2 = new Point(0, 0);      //origin point y
        /// <summary>
        /// Reset point setting
        /// </summary>
        static string tmpa, tmpb, tmp_sr, tmp_save, tmp_fill;
        /// <summary>
        /// tmpa = log.text normal output
        /// tmpb = log.text erro check output
        /// tmp_sr = Handle load and run command read
        /// tmp_save = Handle case save 
        /// tmo_fill = Handle case fill on/off
        /// </summary>
        static Graphics g = null;
      

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
        
        private void FontDialog1_Apply(object sender, EventArgs e)
        {
            
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e) { }
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e) { }
        private void Canvas_Paint(object sender, PaintEventArgs e) { }
        private void textBox1_TextChanged(object sender, EventArgs e) { }
        private void Commandline_TextChanged(object sender, EventArgs e) { }
        private void erroline_TextChanged(object sender, EventArgs e) { }
        private void Form1_Paint(object sender, PaintEventArgs e) { }

        public Form1()
        {
            InitializeComponent();
            Canvas.BackColor = Color.LightGray;
        }
        /// <summary>
        /// Read method;
        /// Read single line and multipline
        /// </summary>
        private void Commandline_KeyDown(object sender, KeyEventArgs e)
        {
            //Graphics g = null;
            g = Canvas.CreateGraphics();
            Random randomNum = new Random();

            if (e.KeyCode == Keys.Enter)
            {
                String command = Commandline.Text;
                string[] str = new string[Commandline.Lines.Length];
                if (Commandline.Lines.Length == 1)
                {
                    Commandline.Clear();
                    Orderexecu0(command);
                }
                else
                {   
                    for (int i = 0; i < Commandline.Lines.Length; i++)
                    {
                        str[i] = Commandline.Lines[i];
                        command = str[i];   // Command Input form Text

                        Orderexecu0(command);
                    }
                }
                Commandline.Clear();
            }
            // End display erro task              
        }
        /// <summary>
        /// Inout value to definehash
        /// </summary>
        /// <param name="param1">CommandArray[0]</param>
        /// <param name="param2">CommandArray[1]</param>
        /// <param name="outInt1">ref from param1</param>
        /// <param name="outInt2">ref from param2</param>
        private void getIfParam(string param1,string param2,ref int outInt1,ref int outInt2)
        {
            if (defineHash.Contains(param1))
            {
                outInt1 = Convert.ToInt32(defineHash[param1].ToString());
            }
            else
            {
                outInt1 = Convert.ToInt32(param1);
            }

            if (defineHash.Contains(param2))
            {
                outInt2 = Convert.ToInt32(defineHash[param2].ToString());
            }
            else
            {
                outInt2 = Convert.ToInt32(param2);
            }
        }

        private List<List<string>> allList = new List<List<string>>();
        private List<string> currentLoopList;
        private Hashtable defineHash = new Hashtable();
        private Hashtable methodHash = new Hashtable();
        private List<string> currentMethodList;
        bool ifRtu = true;
        bool inMethod = false;
        string currentMethdKey = "";

        /// <summary>
        /// Start case define "method","if","loop" and "End" and handle veriable.
        /// </summary>
        private void Orderexecu0(string command)
        {
            command = command.Trim().ToLower();
            string[] commandArray = command.Split(' ');  // Command Arrary to CommandArray
            tmpb = "";
            tmp_sr = "";                                           
            tmp_save = "";
            string cmd = commandArray[0];
            if (string.IsNullOrEmpty(cmd)) return;
            
            if (cmd != "end")
            {
                if (inMethod)
                {
                    tmpa += command + "\r\n";
                    if (tmp_sr == "")
                    {
                        log.Text = "";
                        log.Text += tmpa;
                        log.SelectionStart = log.Text.Length;
                        log.ScrollToCaret();
                        log.Refresh();
                    }
                    ((List<string>)methodHash[currentMethdKey]).Add(command);
                    return;
                }

                if (cmd != "else" && cmd != "if" && ifRtu == false)
                {
                    tmpa += command + "\r\n";
                    if (tmp_sr == "")
                    {
                        log.Text = "";
                        log.Text += tmpa;
                        log.SelectionStart = log.Text.Length;
                        log.ScrollToCaret();
                        log.Refresh();
                    }
                    return;
                }
                

                if (allList.Count > 0)
                {
                    tmpa += command + "\r\n";
                    if (tmp_sr == "")
                    {
                        log.Text = "";
                        log.Text += tmpa;
                        log.SelectionStart = log.Text.Length;
                        log.ScrollToCaret();
                        log.Refresh();
                    }
                    int index = allList.Count - 1;
                    allList[index].Add(command);
                    return;
                }
            }
            
            switch (cmd)
            {          
                case "method":
                    try
                    {
                        string methodKey = commandArray[1];
                        currentMethodList = new List<string>();
                        if (methodHash.ContainsKey(methodKey))
                        {
                            methodHash[methodKey] = currentMethodList;// Replace
                        }
                        else
                        {
                            methodHash.Add(methodKey, currentMethodList);// Add
                        }
                        //currentMethodList.Add(commandArray[2]);
                        currentMethdKey = methodKey;
                        inMethod = true;
                    }
                    catch (Exception)
                    {
                        tmpb = "method command erro";
                    }
                    break;
                case "define":
                    try
                    {
                        int q;
                        if (int.TryParse(commandArray[2], out q))
                        {
                            if (defineHash.ContainsKey(commandArray[1]))
                            {
                                defineHash[commandArray[1]] = q;
                                
                            }
                            else
                            {
                                defineHash.Add(commandArray[1], q);
                            }
                           
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Text = ex.Message; tmpb = ex.Message;
                    }
                    break;
                case "end":
                    try
                    {
                        if (commandArray[1] == "if")
                        {
                            ifRtu = true;
                        }
                        else if (commandArray[1] == "loop")
                        {
                            int index = allList.Count - 1;
                            int loopNum = Convert.ToInt32(allList[index][0]);
                            for (int i = 0; i < loopNum; i++)
                            {
                                for (int j = 1; j < allList[index].Count; j++)
                                {
                                    Orderexecu1(allList[index][j]);
                                }
                            }
                            allList.RemoveAt(index);
                        }
                        else if (commandArray[1] == "method")
                        {
                            inMethod = false;
                        }
                        else
                        { tmpb = "erro 100"; }  
                    }
                    catch (Exception)
                    { tmpb = "End loop command wihout syntax"; }
                    break;
                case "loop":
                    try
                    {
                        myParser.CheckParams_loop(commandArray, ref param1);
                        currentLoopList = new List<string>();
                        currentLoopList.Add(commandArray[1]);
                        allList.Add(currentLoopList);
                    }
                    catch (Exception ex)
                    {
                        log.Text = ex.Message; tmpb = ex.Message;
                    }
                    break;
                case "else":
                    if (commandArray.Length < 2)//else 
                    {
                        ifRtu = !ifRtu;
                        break;
                    }
                    else if (commandArray[1] == "if") 
                    {
                        Orderexecu0(command.Replace("else","").Trim());
                    }
                    break;
                case "if":
                    try
                    {
                        ifRtu = true;
                        int out1=0, out2=0;
                        string paseOrder = command.Replace("if ", "").Replace("and ","").Replace("(","").Replace(")","");
                        string[] orderArray = paseOrder.Split(' ');
                        if (orderArray.Length%3 != 0)
                        {
                            log.Text = "error if"; tmpb = "error if";
                        }
                        int count = orderArray.Length / 3;
                        for (int i=0;i< count; i++)
                        {
                            getIfParam(orderArray[3 * i], orderArray[3 * i + 2], ref out1, ref out2);
                            switch (orderArray[3 * i + 1])
                            {
                                case "==":
                                    if (out1 != out2)
                                    {
                                        ifRtu = false;
                                        break;
                                    }
                                    break;
                                case ">":
                                    if (out1 <= out2)
                                    {
                                        ifRtu = false;
                                        break;
                                    }
                                    break;
                                case ">=":
                                    if (out1 < out2)
                                    {
                                        ifRtu = false;
                                        break;
                                    }
                                    break;
                                case "<":
                                    if (out1 >= out2)
                                    {
                                        ifRtu = false;
                                        break;
                                    }
                                    break;
                                case "<=":
                                    if (out1 > out2)
                                    {
                                        ifRtu = false;
                                        break;
                                    }
                                    break;
                                case "!=":
                                    if (out1 == out2)
                                    {
                                        ifRtu = false;
                                        break;
                                    }
                                    break;
                                default:
                                    ifRtu = false;
                                    break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Text = ex.Message; tmpb = ex.Message;
                    }
                    break;

                default:
                    Orderexecu1(command);
                    break;
            }
            

            if (tmpb != "")
            {
                tmpa += command + "<=Erro Input " + "(" + tmpb + ")" + "\r\n";
            }
            else
            { tmpa += command + "\r\n"; }
            if (tmp_sr == "")
            {
                log.Text = "";
                log.Text += tmpa;
                log.SelectionStart = log.Text.Length;
                log.ScrollToCaret();
                log.Refresh();
            }
        }

        private string getNumValue(string s)
        {
            if (defineHash.Contains(s))
            {
                return defineHash[s].ToString();
            }
            else
                return s;
        }
        /// <summary>
        /// Draw command execution to design pattern
        /// </summary>
        private void Orderexecu1(string command)
        {
            command = command.Trim().ToLower();
            string[] commandArray = command.Split(' ');  // Command Arrary to CommandArray
            tmpb = "";
            tmp_sr = "";
            tmp_save = "";

            string cmd = commandArray[0];
            if (string.IsNullOrEmpty(cmd)) return;
            switch (cmd)
            {
                case "fill":
                    if (commandArray.Length == 2)
                    {
                        if (commandArray[1] == "on")
                        { tmp_fill = "on"; }
                        else if (commandArray[1] == "off")
                        { tmp_fill = "off"; }
                        else
                        { tmpb = "Erro,only available for On/Off"; }
                    }
                    else
                    { tmpb = "Fill command error"; }
                    break;

                case "drawto":
                    try
                    {
                        // parameter error checking 
                        myParser.CheckParams(commandArray, ref param1, ref param2, ref curX, ref curY);
                        SharpFactory sharpFactory = new SharpFactory(g, New.Enum.ESharpType.Line);
                        sharpFactory.MoveTo(curX, curY);
                        sharpFactory.SetPen(myPen);
                        sharpFactory.Draw(param1, param2);
                        
                        curX = param1;//reset endpoint to startpoint
                        curY = param2;//reset endpoint to startpoint
                    }
                    catch (Exception ex)
                    { tmpb = ex.Message; }
                    break;
                case "moveto":
                    try
                    {    // parameter error checking
                        myParser.CheckParams(commandArray, ref param1, ref param2, ref curX, ref curY);
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawEllipse(erasePos, curX - 7, curY - 7, 14, 14);
                        g.DrawEllipse(erasePos, curX - 9, curY - 9, 18, 18);
                        g.DrawLine(erasePos, curX - 15, curY, curX + 15, curY);
                        g.DrawLine(erasePos, curX, curY - 15, curX, curY + 15);
                        
                        curX = param1;
                        curY = param2;
                        g.DrawLine(myPen, param1, param2, param1, param2);
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawEllipse(myPos, curX - 7, curY - 7, 14, 14);
                        g.DrawEllipse(myPos, curX - 9, curY - 9, 18, 18);
                        g.DrawLine(myPos, curX - 15, curY, curX + 15, curY);
                        g.DrawLine(myPos, curX, curY - 15, curX, curY + 15);
                    }
                    catch (Exception ex)
                    { log.Text = ex.Message; tmpb = ex.Message; }
                    break;

                case "rectangle":
                    try
                    {
                        // parameter error checking
                        commandArray[1] = getNumValue(commandArray[1]);
                        commandArray[2] = getNumValue(commandArray[2]);
                        myParser.CheckParams(commandArray, ref param1, ref param2, ref curX, ref curY);
                        SharpFactory sharpFactory = new SharpFactory(g, New.Enum.ESharpType.Rectangle);
                        sharpFactory.MoveTo(curX, curY);
                        sharpFactory.SetPen(myPen);
                        sharpFactory.SetFill(tmp_fill);
                        sharpFactory.Draw(param1, param2);

                        curX += param1;
                        curY += param2;
                    }
                    catch (Exception ex)
                    { log.Text = ex.Message; tmpb = ex.Message; }
                    break;

                case "circle":
                    try
                    {    // parameter error checking
                        commandArray[1] = getNumValue(commandArray[1]);
                        myParser.CheckParams_circle(commandArray, ref param1);
                        SharpFactory sharpFactory = new SharpFactory(g, New.Enum.ESharpType.Circle);
                        sharpFactory.MoveTo(curX, curY);
                        sharpFactory.SetPen(myPen);
                        sharpFactory.SetFill(tmp_fill);
                        sharpFactory.Draw(param1, param1);
                        curX += param1;
                        curY += param2;

                    }
                    catch (Exception ex)
                    { log.Text = ex.Message; tmpb = ex.Message; }
                    break;
                case "triangle":
                    try
                    {    // parameter error checking
                        myParser.CheckParams_trian(commandArray, ref param1, ref param2, ref param3, ref param4, ref param5, ref param6);
                        Point point1 = new Point(param1, param2);
                        Point point2 = new Point(param3, param4);
                        Point point3 = new Point(param5, param6);
                        Point[] curvePoints = { point1, point2, point3 };
                        SharpFactory sharpFactory = new SharpFactory(g, New.Enum.ESharpType.Triangle);
                        sharpFactory.MoveTo(param1, param2);
                        sharpFactory.SetPen(myPen);
                        sharpFactory.SetFill(tmp_fill);
                        sharpFactory.Draw(curvePoints);
                        curX = param5;
                        curY = param6;
                    }
                    catch (Exception ex)
                    { log.Text = ex.Message; tmpb = ex.Message; }
                    break;
                case "reset":
                    try
                    {
                        myParser.CheckParams_other(commandArray);
                        g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                        g.DrawEllipse(erasePos, curX - 7, curY - 7, 14, 14);
                        g.DrawEllipse(erasePos, curX - 9, curY - 9, 18, 18);
                        g.DrawLine(erasePos, curX - 15, curY, curX + 15, curY);
                        g.DrawLine(erasePos, curX, curY - 15, curX, curY + 15);
                        g.DrawLine(myPen, origin1, origin2);
                        curX = 0;
                        curY = 0;
                        g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                        g.DrawEllipse(myPos, curX - 7, curY - 7, 14, 14);
                        g.DrawEllipse(myPos, curX - 9, curY - 9, 18, 18);
                        g.DrawLine(myPos, curX - 15, curY, curX + 15, curY);
                        g.DrawLine(myPos, curX, curY - 15, curX, curY + 15);
                    }
                    catch (Exception ex)
                    { log.Text = ex.Message; tmpb = ex.Message; }
                    break;

                case "clear":
                    try
                    {
                        myParser.CheckParams_other(commandArray);
                        Canvas.Refresh();
                    }
                    catch (Exception ex)
                    { log.Text = ex.Message; tmpb = ex.Message; }
                    break;

                case "exit":
                    try
                    {
                        myParser.CheckParams_other(commandArray);
                        if (System.Windows.Forms.Application.MessageLoop)
                        {
                            System.Windows.Forms.Application.Exit();
                        }
                    }
                    catch (Exception ex)
                    { log.Text = ex.Message; tmpb = ex.Message; }
                    break;

                case "save":
                    try
                    {
                        {
                            saveFileDialog1.Filter = "Text File(txt)|.txt";
                            saveFileDialog1.FileName = String.Empty;
                            saveFileDialog1.DefaultExt = ".txt";
                            DialogResult result = saveFileDialog1.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                                StreamWriter writer = new StreamWriter(fs);
                                writer.Write(tmpa);
                                tmp_save = saveFileDialog1.FileName;
                                MessageBox.Show("File Save:" + tmp_save);
                                writer.Close();
                                tmpa = "";
                            }
                        }
                    }
                    catch (Exception ex)
                    { log.Text = ex.Message; tmpb = ex.Message; }
                    break;
                case "load":
                    try
                    {
                        openFileDialog1.InitialDirectory = "c:\\";
                        openFileDialog1.Filter = "Text File(txt)|*.txt";
                        openFileDialog1.FilterIndex = 2;
                        openFileDialog1.RestoreDirectory = true;

                        if (openFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            String sr_filePath, sr_fileContent;
                            sr_filePath = openFileDialog1.FileName;
                            //Read the contents of the file into a stream
                            var fileStream = openFileDialog1.OpenFile();
                            using (StreamReader reader = new StreamReader(fileStream))
                            {
                                sr_fileContent = reader.ReadToEnd();
                                tmp_sr = sr_fileContent;
                                log.Text = sr_fileContent;
                                tmpa = tmp_sr;
                            }
                        }
                    }

                    catch (Exception)
                    { tmpa = "Successful to load file."; }
                    break;
                case "run":
                    try
                    {
                        openFileDialog1.InitialDirectory = "c:\\";
                        openFileDialog1.Filter = "Text File(txt)|*.txt";
                        openFileDialog1.FilterIndex = 2;
                        openFileDialog1.RestoreDirectory = true;

                        if (openFileDialog1.ShowDialog() == DialogResult.OK)
                        {
                            String sr_filePath;

                            sr_filePath = openFileDialog1.FileName;

                            //Read the contents of the file into a stream
                            var fileStream = openFileDialog1.OpenFile();

                            using (StreamReader reader = new StreamReader(fileStream))
                            {
                                string[] prog = File.ReadAllLines(sr_filePath);
                                for (int i = 0; i < prog.Length; i++)
                                {
                                    if (string.IsNullOrEmpty(prog[i])) continue;  //skip blank record
                                    command = prog[i];
                                    Orderexecu0(command);
                                    
                                }
                            }
                        }
                    }
                    catch (Exception)
                    { log.Text = "Successfully to run"; }       
                    break;

                case "help":
                    try
                    {
                        switch (commandArray[1])
                            {                     
                            case "-n":
                                String first_2 = "*****Command Syntax:(xx-TX position,yy-TY position)*****" + "\r\n";
                                String sy = String.Format("{0,-10} | {1,-10} | {2,5}", "Command", "Peremeters", "Description") + "\r\n";
                                String stm = String.Format("{0,-10} | {1,-10} | {2,5}", "moveto", "xx yy", "Move to start point when you start") + "\r\n";
                                String std = String.Format("{0,-10}  | {1,-10} | {2,5}", "drawto", "xx yy", "Draw line function") + "\r\n";
                                String stc = String.Format("{0,-12}   | {1,-11} | {2,5}", "circle", "xx", "Draw circle function(Radius)") + "\r\n";
                                String str = String.Format("{0,-10}  | {1,-10} | {2,5}", "rectangle", "xx yy", " Draw rectangle function") + "\r\n";
                                String stt = String.Format("{0,-10}  | {1,-10}h | {2,5}", "triangle", "tg1,tg2,tg3", "Draw triangle within xx yy for each tg") + "\r\n";
                                tmpa = first_2 + sy + std + stc + str + stt;
                                break;

                            case "-l":
                                String first_3 = "*****Wellcome to Loop function help*****" + "\r\n";
                                String l_1 = String.Format("{0,-40} | {1,-10} ", "[define][variable][value]", "Define value") + "\r\n";
                                String l_2 = String.Format("{0,-48} | {1,-10} ", "[loop][times]", "Define loop time") + "\r\n";
                                String l_3 = String.Format("{0,-32} | {1,-10} ", "[Draw_command][variable]", "Sample:circle x") + "\r\n";
                                String l_4 = String.Format("{0,-43} | {1,-10} ", "[variable][+/-][value]", "Sample:x + 10") + "\r\n";
                                String l_5 = String.Format("{0,-48} | {1,-10} ", "[end loop]", "End function within loop") + "\r\n";
                                tmpa = first_3 + l_1 + l_2 + l_3 + l_4 + l_5;
                                break;

                            case "-if":
                                String first_4 = "*****Wellcome to If function help*****" + "\r\n";
                                String if_1 = String.Format("{0,-40} | {1,-10} ", "[define][variable][value]", "Define value") + "\r\n";
                                String if_2 = String.Format("{0,-48} | {1,-10} ", "[if]([condition])", "Support multiline") + "\r\n";
                                String if_3 = String.Format("{0,-32} | {1,-10} ", "[Draw_command][variable]", "Sample:circle x") + "\r\n";
                                String if_4 = String.Format("{0,-44} | {1,-10} ", "[else if]([condition])", "Support multiline") + "\r\n";
                                String if_5 = String.Format("{0,-32} | {1,-10} ", "[Draw_command][variable]", "Sample:circle x") + "\r\n";
                                String if_6 = String.Format("{0,-44} | {1,-10} ", "[else]([condition])", "Support multiline") + "\r\n";
                                String if_7 = String.Format("{0,-32} | {1,-10} ", "[Draw_command][variable]", "Sample:circle x") + "\r\n";
                                String if_8 = String.Format("{0,-52} | {1,-10} ", "[end if]", "End function within if") + "\r\n";
                                tmpa = first_4 + if_1 + if_2 + if_3 + if_4 + if_5 +if_6 + if_7 + if_8;
                                break;

                            case "-m":
                                String first_5 = "*****Wellcome to Method function help*****" + "\r\n";
                                String m_1 = String.Format("{0,-30} | {1,-10} ", "[method][name]([parameters])", "Sample:define demo(x,y)") + "\r\n";
                                String m_2 = String.Format("{0,-27} | {1,-10} ", "[Draw_command][parameters]", "Sample:rectangle x y") + "\r\n";
                                String m_3 = String.Format("{0,-43} | {1,-10} ", "[end method]", "End function within method") + "\r\n";
                                String m_4 = String.Format("{0,-36} | {1,-10} ", "[method name][value]", "Sample:demo(20,100)") + "\r\n";
                                tmpa = first_5 + m_1 + m_2 + m_3 + m_4;
                                break;

                            default:
                                tmpb = "Help command peremetres erro";
                                break;
                        }                     
                    }
                    catch(Exception)
                    {
                        String first = "*****Wellcome to Points Program Help*****" + "\r\n";
                        String mu = String.Format("{0,-10} | {1,-10} ", "Command", "Description") + "\r\n";
                        String mu1 = String.Format("{0,-15} | {1,-10} ", "[help][-n]", "Normal command drawing") + "\r\n";
                        String mu2 = String.Format("{0,-16} | {1,-10} ", "[help][-l]", "Loop function with variable command drawing") + "\r\n";
                        String mu3 = String.Format("{0,-17} | {1,-10} ", "[help][-if]", "If function command drawing") + "\r\n";
                        String mu4 = String.Format("{0,-15} | {1,-10} ", "[help][-m]", "Method function command drawing") + "\r\n";
                        tmpa = first + mu + mu1 + mu2 + mu3 + mu4;
                    }
                    break;

                case "pen":
                    try
                    {
                        myParser.CheckParams_pen(commandArray);
                        myPen.Color = Color.FromName(commandArray[1]);
                    }
                    catch (Exception ex)
                    { tmpb = ex.Message; }
                    break;
                default:
                    // Processing methods 
                    try
                    {
                        if (methodHash.ContainsKey(commandArray[0]))// define variable 
                        {
                            string[] param = commandArray[1].Replace("(", "").Replace(")", "").Split(',');// opetaion variable
                            int paramCount = param.Length;
                            int index = 0;
                            foreach (string method in ((List<string>)methodHash[commandArray[0]]))
                            {
                                int paramNum2 = method.Split(' ').Length - 1;//reference for standard
                                string realMethd = method.Split(' ')[0];
                                if (paramNum2 > paramCount)
                                {
                                    continue;
                                }
                                string order = realMethd;
                                for (int i = 0; i < paramNum2; i++)
                                {
                                    order = order + " " + param[(index++) % paramCount];
                                }
                                Orderexecu1(order);
                            }
                        }
                        else
                        {
                            tmpb = "Command name erro";
                        }
                        // Processing cal variable issue
                        switch (commandArray[1])
                        {
                            case "+":
                                defineHash[commandArray[0]] = Convert.ToInt32(defineHash[commandArray[0]]) + Convert.ToInt32(commandArray[2]);
                                break;

                            case "-":
                                defineHash[commandArray[0]] = Convert.ToInt32(defineHash[commandArray[0]]) - Convert.ToInt32(commandArray[2]);
                                break;

                        }
                    }
                    catch(Exception) {}
                    
                    break;
            } //End switch cmd
        } //End execute metod
    } //End Class
} //End namespace


