﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Library
{
    /// <summary>
    /// CheckParams check input with case rectangle,drawto and moveto.
    /// </summary>
    /// <param name="command">Command.length must be 3</param>
    /// <param name="arg1">New define X position</param>
    /// <param name="arg2">New define Y position</param>
    /// <param name="X">current X position</param>
    /// <param name="Y">current Y position</param>
    public class CommandParser
    {
        public void CheckParams(string[] command, ref int arg1, ref int arg2,ref int X,ref int Y)
        {
            if (command.Length != 1)
            {
                if (command.Length != 3)
                {
                    throw new Exception("Commannd parameters error");
                }

                else if (!int.TryParse(command[1], out arg1))
                {
                    throw new Exception("Command parameters 1 error");
                }
                else if (!int.TryParse(command[2], out arg2))
                {
                    throw new Exception("Command parameters 2 error");
                }
                else if (arg1 < -1 || arg2 < -1 || arg1 == -0 || arg2 == -0 || arg1 == -1 || arg2 == -1)
                {
                    throw new Exception("Value should more than 0");
                }
                else if (arg1 > 670 || arg2 > 320)
                {
                    throw new Exception("Scope is out of range.X,Y.Max(670,320)");
                }
                if (command[0] != "rectangle")
                {
                 if (X == arg1 && Y == arg2)
                    {
                        throw new Exception("X and Y point should not same with previous point");
                    } }
            }
            else if (command[0]!= "drawto" && command[0] != "moveto" && command[0] != "rectangle")
            {
                throw new Exception("Element erro");
            }
            else if (arg1 < -1 || arg2 < -1 || arg1 == -0 || arg2 == -0 || arg1 == -1 || arg2 == -1)
            {
                throw new Exception("Value should more than 0");
            }
            else if (arg1 > 670 || arg2 > 320)
            {
                throw new Exception("Scope is out of range.X,Y.Max(670,320)");
            }
            else if (X == arg1 && Y == arg2)
            {
                throw new Exception("X and Y point should not same with previous point");
            }

        }// End CheckParams

        /// <summary>
        /// CheckParams_circle mainly for case circle input checking 
        /// </summary>
        /// <param name="command">Command.length must be 2</param>
        /// <param name="arg1">Circle radius</param>
        public void CheckParams_circle(string[] command, ref int arg1)
        {
                if (command.Length !=1) 
            {
                if (command.Length != 2)
                {
                    throw new Exception("Commannd parameters error,radius ");
                }

                else if (!int.TryParse(command[1], out arg1))
                {
                    throw new Exception("Command parameters 1 error");
                }
                else if (arg1 < 0 || arg1 == -0 || arg1 == -1)
                {
                    throw new Exception("Value should more than 0");
                }
                else if (arg1 > 165)
                {
                    throw new Exception("Scope is out of range.Radius should <165");
                }
             }

            else if (arg1 < 0 || arg1 == -0 || arg1 == -1)
            {
                    throw new Exception("Value should more than 0");
                }
            else if (arg1 > 165)
                {
                    throw new Exception("Scope is out of range.Radius should <165");
                }
            else if (command[0] != "circle")
            {
                throw new Exception("Element Erro");
            }
           
              }//method
        /// <summary>
        /// CheckParams_trian mainly for checking case trinagle
        /// </summary>
        /// <param name="command">command.length must be 7</param>
        /// <param name="arg1">triangle point 1</param>
        /// <param name="arg2">triangle point 2</param>
        /// <param name="arg3">triangle point 3</param>
        /// <param name="arg4">triangle point 4</param>
        /// <param name="arg5">triangle point 5</param>
        /// <param name="arg6">triangle point 6</param>
        public void CheckParams_trian(string[] command, ref int arg1, ref int arg2, ref int arg3, ref int arg4, ref int arg5, ref int arg6)
        {
            if (command.Length != 1)
            {
                if (command.Length != 7)
                {
                    throw new Exception("Commannd parameters error");
                }
                else if (!int.TryParse(command[1], out arg1))
                {
                    throw new Exception("Command parameters 1 error");
                }
                else if (!int.TryParse(command[2], out arg2))
                {
                    throw new Exception("Command parameters 2 error");
                }
                else if (!int.TryParse(command[3], out arg3))
                {
                    throw new Exception("Command parameters 3 error");
                }
                else if (!int.TryParse(command[4], out arg4))
                {
                    throw new Exception("Command parameters 4 error");
                }
                else if (!int.TryParse(command[5], out arg5))
                {
                    throw new Exception("Command parameters 5 error");
                }
                else if (!int.TryParse(command[6], out arg6))
                {
                    throw new Exception("Command parameters 6 error");
                }
                else if (arg1 < 0 || arg2 < 0 || arg3 < 0 || arg4 < 0 || arg5 < 0 || arg6 < 0 || arg1 == -0 || arg2 == -0 || arg3 == -0 || arg4 == -0 || arg5 == -0 || arg6 == -0 || arg1 == -1 || arg2 == -1 || arg3 == -1 || arg4 == -1 || arg5 == -1 || arg6 == -1)
                {
                    throw new Exception("Value should more than 0");
                }
                else if (arg1 > 670 || arg2 > 320 || arg3 > 670 || arg4 > 320 || arg5 > 670 || arg6 > 320)
                {
                    throw new Exception("Scope is out of range.X,Y.Max(670,320)");
                }
                else if (arg1 == arg3 && arg3 == arg5 && arg1 == arg5)
                {
                    throw new Exception("Can't same position of X");
                }
                else if (arg2 == arg4 && arg4 == arg6 && arg2 == arg6)
                {
                    throw new Exception("Can't same position of Y");
                }
                else if (arg1 == arg3 && arg2 == arg4 && arg3 == arg6)
                {
                    throw new Exception("Triangle points access on the same line,please try differnt points of X,Y");
                }
            }
            else if (command[0]!="triangle")
            {
                throw new Exception("Element erro");
            }
            else if (arg1 < 0 || arg2 < 0 || arg3 < 0 || arg4 < 0 || arg5 < 0 || arg6 < 0 || arg1 == -0 || arg2 == -0 || arg3 == -0 || arg4 == -0 || arg5 == -0 || arg6 == -0 || arg1 == -1 || arg2 == -1 || arg3 == -1 || arg4 == -1 || arg5 == -1 || arg6 == -1)
            {
                throw new Exception("Value should more than 0");
            }
            else if (arg1 > 670 || arg2 > 320)
            {
                throw new Exception("Scope is out of range.X,Y.Max(670,320)");
            }
            else if (arg1 == arg3 && arg3 == arg5 && arg1 == arg5)
            {
                throw new Exception("Can't same position of X");
            }
            else if (arg2 == arg4 && arg4 == arg6 && arg2 == arg6)
            {
                throw new Exception("Can't same position of Y");
            }
            else if (arg1 == arg3 && arg2 == arg4 && arg3 == arg6)
            {
                throw new Exception("Triangle points access on the same line,please try differnt points of X,Y");
            }
        }//End CheckParams_trian

        /// <summary>
        /// CheckParams_other mainly for checking case run,load,save,reset,clear,exit
        /// </summary>
        /// <param name="command">command.length must be 1</param>
        public void CheckParams_other(string[] command)
        {
            if (command.Length != 1)
            {
                throw new Exception("Commannd parameters error");
            }

            if (command[0] != "reset"  && command[0] != "run" && command[0] != "save" && command[0] != "load" && command[0] != "exit" && command[0] != "clear")
            {
                throw new Exception("command elelment erro");
            }

        }//End CheckParams_other

        /// <summary>
        /// CheckParams_pen mainaly for check case pen input
        /// </summary>
        /// <param name="command">command.length must be 2 </param>
        public void CheckParams_pen(string[] command)
        {
            if (command.Length != 2)
            {
                throw new Exception("Commannd parameters error");
            }

         }//End CheckParams_pen

        /// <summary>
        /// CheckParams_pen mainaly for check case loop input
        /// </summary>
        /// <param name="command">command.length must be 2</param>
        /// <param name="arg1">loop command parametres must be integer</param>
        public void CheckParams_loop(string[] command, ref int arg1)
        {
            if (command.Length != 2)
            {
                throw new Exception("Loop Commannd parameters error");
            }

            else if (!int.TryParse(command[1], out arg1))
            {
                throw new Exception("Loop Command parametres must wihin Integer");
            }
        }//End CheckParams_loop

        public void CheckParams_help(string[] command)
        {
            
        }//End CheckParams_loop
    }
}

