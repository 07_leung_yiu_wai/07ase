﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Windows.Forms.VisualStyles;
using Parser.Library;


namespace Assessment1
{
    public partial class Form1 : Form
    {
        static int curX = 0, curY = 0, param1 = 0, param2 = 0, param3 = 0, param4 = 0, param5 = 0, param6 = 0;
        static Pen myPen = new Pen(Color.Black, 2);
        static Pen myPos = new Pen(Color.Red, 1);
        static Pen erasePos = new Pen(Color.LightGray, 2);
        static CommandParser myParser = new CommandParser();
        static Point origin1 = new Point(0, 0);                  //origin point x 
        static Point origin2 = new Point(0, 0);                  //origin point y
        static string tmpa, tmpb, tmp_sr, tmp_save, tmp_fill;         //tmpa from command data;tmpb decide "Error Message" to log.Text;tmop_sr for IO.Reader;tmp_save for IO save file path;tmp_fill for veriable of fill colour. 

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e) { }
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e) { }
        private void Canvas_Paint(object sender, PaintEventArgs e) { }
        private void textBox1_TextChanged(object sender, EventArgs e) { }
        private void Commandline_TextChanged(object sender, EventArgs e) { }
        private void erroline_TextChanged(object sender, EventArgs e) { }
        private void Form1_Paint(object sender, PaintEventArgs e) { }
       
            public Form1()
            {
                InitializeComponent();
                Canvas.BackColor = Color.LightGray;
            }

            private void Commandline_KeyDown(object sender, KeyEventArgs e)
            {
                Graphics g = null;
                g = Canvas.CreateGraphics();
                Random randomNum = new Random();

                if (e.KeyCode == Keys.Enter)
                {
                    string command = Commandline.Text;   // Command Input form Text
                    command = command.ToLower();
                    Commandline.Clear();
                    string[] commandArray = command.Split(' ');  // Command Arrary to CommandArray
                    tmpb = "";
                    tmp_sr = "";
                    tmp_save = "";

                    switch (commandArray[0])
                    {
                        case "fill":                                
                            if (commandArray.Length == 2)
                            {
                                if (commandArray[1] == "on")
                                { tmp_fill = "on"; }
                                else if (commandArray[1] == "off")
                                { tmp_fill = "off"; }
                                else
                                { tmpb = "Erro,only available for On/Off"; }
                            }
                            else
                            { tmpb = "Command error"; }
                            break;

                        case "drawto":                                
                            try
                            {
                                // parameter error checking
                                myParser.CheckParams(commandArray, ref param1, ref param2, ref curX, ref curY);
                                g.DrawLine(myPen, curX, curY, param1, param2);
                                curX = param1;
                                curY = param2;
                            }
                            catch (Exception ex)
                            { tmpb = ex.Message; }
                            break;
                        case "moveto":
                            try
                            {    // parameter error checking
                                myParser.CheckParams(commandArray, ref param1, ref param2, ref curX, ref curY);
                                g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                                g.DrawEllipse(erasePos, curX - 7, curY - 7, 14, 14);
                                g.DrawEllipse(erasePos, curX - 9, curY - 9, 18, 18);
                                g.DrawLine(erasePos, curX - 15, curY, curX + 15, curY);
                                g.DrawLine(erasePos, curX, curY - 15, curX, curY + 15);
                                g.DrawLine(myPen, param1, param2, param1, param2);
                                curX = param1;
                                curY = param2;
                                g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                                g.DrawEllipse(myPos, curX - 7, curY - 7, 14, 14);
                                g.DrawEllipse(myPos, curX - 9, curY - 9, 18, 18);
                                g.DrawLine(myPos, curX - 15, curY, curX + 15, curY);
                                g.DrawLine(myPos, curX, curY - 15, curX, curY + 15);
                            }
                            catch (Exception ex)
                            { log.Text = ex.Message; tmpb = ex.Message; }
                            break;
                        case "rectangle":
                            try
                            {    // parameter error checking
                                myParser.CheckParams(commandArray, ref param1, ref param2, ref curX, ref curY);
                                if (tmp_fill == "on")
                                {
                                    SolidBrush blueBrush = new SolidBrush(Color.Blue);
                                    g.FillRectangle(blueBrush, curX, curY, param1, param2);
                                }
                                else if (tmp_fill == "off" || tmp_fill == null)
                                {
                                    g.DrawRectangle(myPen, curX, curY, param1, param2);
                                }
                                curX = curX + param1;
                                curY = curY + param2;
                            }
                            catch (Exception ex)
                            { log.Text = ex.Message; tmpb = ex.Message; }
                            break;

                        case "circle":
                            try
                            {    // parameter error checking
                                myParser.CheckParams_circle(commandArray, ref param1);
                            if (tmp_fill == "on")
                            {
                                SolidBrush blueBrush = new SolidBrush(Color.Blue);
                                g.FillEllipse(blueBrush, curX, curY, param1 * 2, param1 * 2);
                            }
                            else if (tmp_fill == "off" || tmp_fill == null)
                            {
                                g.DrawEllipse(myPen, curX, curY, param1 * 2, param1 * 2);
                            }
                            }
                            catch (Exception ex)
                            { log.Text = ex.Message; tmpb = ex.Message; }
                            break;

                        case "triangle":
                            try
                            {    // parameter error checking
                                myParser.CheckParams_trian(commandArray, ref param1, ref param2, ref param3, ref param4, ref param5, ref param6);
                                Point point1 = new Point(param1, param2);
                                Point point2 = new Point(param3, param4);
                                Point point3 = new Point(param5, param6);
                                Point[] curvePoints = { point1, point2, point3 };
                                if (tmp_fill == "on")
                                {
                                    SolidBrush blueBrush = new SolidBrush(Color.Blue);
                                    g.FillPolygon(blueBrush, curvePoints);
                                }
                            else if (tmp_fill == "off" || tmp_fill == null)
                                { g.DrawPolygon(myPen, curvePoints); }
                            }
                            catch (Exception ex)
                            { log.Text = ex.Message; tmpb = ex.Message; }
                            break;

                        case "reset":
                            try
                            {
                                myParser.CheckParams_other(commandArray);
                                g.DrawEllipse(erasePos, curX - 5, curY - 5, 10, 10);
                                g.DrawEllipse(erasePos, curX - 7, curY - 7, 14, 14);
                                g.DrawEllipse(erasePos, curX - 9, curY - 9, 18, 18);
                                g.DrawLine(erasePos, curX - 15, curY, curX + 15, curY);
                                g.DrawLine(erasePos, curX, curY - 15, curX, curY + 15);
                                g.DrawLine(myPen, origin1, origin2);
                                curX = 0;
                                curY = 0;
                                g.DrawEllipse(myPos, curX - 5, curY - 5, 10, 10);
                                g.DrawEllipse(myPos, curX - 7, curY - 7, 14, 14);
                                g.DrawEllipse(myPos, curX - 9, curY - 9, 18, 18);
                                g.DrawLine(myPos, curX - 15, curY, curX + 15, curY);
                                g.DrawLine(myPos, curX, curY - 15, curX, curY + 15);
                            }
                            catch (Exception ex)
                            { log.Text = ex.Message; tmpb = ex.Message; }
                            break;

                        case "clear":
                            try
                            {
                                myParser.CheckParams_other(commandArray);
                                Canvas.Refresh();
                            }
                            catch (Exception ex)
                            { log.Text = ex.Message; tmpb = ex.Message; }
                            break;

                        case "exit":
                            try
                            {
                                myParser.CheckParams_other(commandArray);
                                if (System.Windows.Forms.Application.MessageLoop)
                                {
                                    System.Windows.Forms.Application.Exit();
                                }
                            }
                            catch (Exception ex)
                            { log.Text = ex.Message; tmpb = ex.Message; }
                            break;

                        case "save":
                            try
                            {
                                {
                                    saveFileDialog1.Filter = "Text File(txt)|.txt";
                                    saveFileDialog1.FileName = String.Empty;
                                    saveFileDialog1.DefaultExt = ".txt";
                                    DialogResult result = saveFileDialog1.ShowDialog();
                                    if (result == DialogResult.OK)
                                    {
                                        FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                                        StreamWriter writer = new StreamWriter(fs);
                                        writer.Write(tmpa);
                                        tmp_save = saveFileDialog1.FileName;
                                        MessageBox.Show("File Save:" + tmp_save);
                                        writer.Close();
                                        tmpa = "";
                                    }
                                }
                            }
                            catch (Exception ex)
                            { log.Text = ex.Message; tmpb = ex.Message; }
                            break;
                        case "load":
                            try
                            {
                                openFileDialog1.InitialDirectory = "c:\\";
                                openFileDialog1.Filter = "Text File(txt)|*.txt";
                                openFileDialog1.FilterIndex = 2;
                                openFileDialog1.RestoreDirectory = true;

                                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                                {
                                    String sr_filePath, sr_fileContent;
                                    sr_filePath = openFileDialog1.FileName;
                                    //Read the contents of the file into a stream
                                    var fileStream = openFileDialog1.OpenFile();
                                    using (StreamReader reader = new StreamReader(fileStream))
                                    {
                                        sr_fileContent = reader.ReadToEnd();
                                        tmp_sr = sr_fileContent;
                                        log.Text = sr_fileContent;
                                        tmpa = tmp_sr;
                                    }
                                }



                            }

                            catch (Exception ex)
                            { log.Text = ex.Message; tmpb = ex.Message; }
                            break;
                        case "run":
                            try
                            {
                                openFileDialog1.InitialDirectory = "c:\\";
                                openFileDialog1.Filter = "Text File(txt)|*.txt";
                                openFileDialog1.FilterIndex = 2;
                                openFileDialog1.RestoreDirectory = true;

                                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                                {
                                    String sr_filePath;

                                    sr_filePath = openFileDialog1.FileName;

                                    //Read the contents of the file into a stream
                                    var fileStream = openFileDialog1.OpenFile();

                                    using (StreamReader reader = new StreamReader(fileStream))
                                    {
                                        string[] prog = File.ReadAllLines(sr_filePath);
                                        for (int i = 0; i <= prog.Length; i++)
                                        {
                                            Commandline.Text = prog[i];
                                            SendKeys.Send("{ENTER}");
                                        }
                                    }
                                }
                            }

                            catch (Exception ex)
                            { log.Text = ex.Message; tmpb = ex.Message; }
                            break;
                        case "pen":
                            try
                            {
                                if (commandArray.Length == 2)
                                {
                                    switch (commandArray[1])
                                    {
                                        case "red":
                                            myPen.Color = Color.Red;
                                            break;
                                        case "blue":
                                            myPen.Color = Color.Blue;
                                            break;
                                        case "orange":
                                            myPen.Color = Color.Orange;
                                            break;
                                        case "green":
                                            myPen.Color = Color.Green;
                                        break;
                                    case "black":
                                        myPen.Color = Color.Black;
                                        break;
                                    default:
                                            myPen.Color = Color.Black;
                                            tmpb = "Error,color only available for green,red,blue and orange";
                                            break;
                                    }
                                }
                                else
                                {
                                    tmpb = "Command error";
                                }
                            }

                            catch (Exception ex)
                            { log.Text = ex.Message; tmpb = ex.Message; }
                            break;
                        default:
                            log.Text = "Element input erro";
                            tmpb = log.Text;
                            break;

                    }
                    // Log.Text to display commandline erro and log
                    if (tmpb != "")
                    {
                        tmpa += command + "<=Erro Input " + "(" + tmpb + ")" + "\r\n";
                    }
                    else
                    { tmpa += command + "\r\n"; }
                    if (tmp_sr == "")
                    {
                        log.Text = "";
                        log.Text += tmpa;
                        log.SelectionStart = log.Text.Length;
                        log.ScrollToCaret();
                        log.Refresh();

                    }
                }
                // End display erro task              
            }
        }
    }



