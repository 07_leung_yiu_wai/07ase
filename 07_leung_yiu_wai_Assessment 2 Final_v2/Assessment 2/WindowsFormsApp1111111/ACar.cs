﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assessment1
{
    public abstract class ACar
    {
        public string Make { get; set; }
        public string EngineSize { get; set; }
        public string Colour { get; set; }
    } // abstract
}
