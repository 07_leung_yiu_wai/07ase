﻿namespace Assessment1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.Canvas = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Commandline = new System.Windows.Forms.TextBox();
            this.log = new System.Windows.Forms.TextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.Canvas.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Canvas
            // 
            this.Canvas.Controls.Add(this.panel2);
            this.Canvas.Controls.Add(this.log);
            this.Canvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Canvas.Location = new System.Drawing.Point(0, 0);
            this.Canvas.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Canvas.Name = "Canvas";
            this.Canvas.Size = new System.Drawing.Size(1443, 986);
            this.Canvas.TabIndex = 0;
            this.Canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.Canvas_Paint);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Commandline);
            this.panel2.Location = new System.Drawing.Point(0, 916);
            this.panel2.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1447, 64);
            this.panel2.TabIndex = 1;
            // 
            // Commandline
            // 
            this.Commandline.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Commandline.Font = new System.Drawing.Font("PMingLiU", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Commandline.Location = new System.Drawing.Point(0, 0);
            this.Commandline.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Commandline.Name = "Commandline";
            this.Commandline.Size = new System.Drawing.Size(1447, 65);
            this.Commandline.TabIndex = 0;
            this.Commandline.Tag = "";
            this.Commandline.TextChanged += new System.EventHandler(this.Commandline_TextChanged);
            this.Commandline.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Commandline_KeyDown);
            // 
            // log
            // 
            this.log.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.log.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.log.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.log.Font = new System.Drawing.Font("PMingLiU", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.log.ForeColor = System.Drawing.Color.White;
            this.log.ImeMode = System.Windows.Forms.ImeMode.On;
            this.log.Location = new System.Drawing.Point(0, 640);
            this.log.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.log.Size = new System.Drawing.Size(1443, 278);
            this.log.TabIndex = 2;
            this.log.TabStop = false;
            this.log.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1443, 986);
            this.Controls.Add(this.Canvas);
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.Canvas.ResumeLayout(false);
            this.Canvas.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.Panel Canvas;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox Commandline;
        private System.Windows.Forms.TextBox log;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

