﻿using System;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parser.Library;


namespace ParserTest
{
    [TestClass]
    public class Parser_Test2
    {
        [TestMethod]
        public void Test_CheckParams_other()
        {
            Exception ex = null;
            try
            {
            //Arrange
                CommandParser cp = new CommandParser();
                String command_1 = "run";
                String[] command = { command_1 };

            //Act
                cp.CheckParams_other(command);
            }

            //Assert
            catch (Exception e)
            {
                ex = e;
            }
            Assert.IsNull(ex, "Erro");
        }
        // End Test_CheckParams_other

        [TestMethod]
        public void Test_CheckParams_circle()
        {
            {
                Exception ex = null;
                try
                {
                    CommandParser cp = new CommandParser();

                //Arrange
                    String command_1 = "circle";
                    String b1 = "22";
                    int arf1 = int.Parse(b1);
                    String []command = { command_1 };

                //Act
                    cp.CheckParams_circle(command, ref arf1);
                }

                //Assert
                catch (Exception e)
                {
                    ex = e;
                }
                Assert.IsNull(ex, "Erro");


            }// End Test_CheckParams_circle

        }

        [TestMethod]
        public void Test_CheckParams_trians()
        {
            {
                Exception ex = null;
                try
                {
                //Arrange
                    CommandParser cp = new CommandParser();
                    String command_1 = "triangle";
                    String b1 = "22"; int arf1 = int.Parse(b1);
                    String b2 = "33"; int arf2 = int.Parse(b2);
                    String b3 = "44"; int arf3 = int.Parse(b3);
                    String b4 = "55"; int arf4 = int.Parse(b4);
                    String b5 = "66"; int arf5 = int.Parse(b5);
                    String b6 = "77"; int arf6 = int.Parse(b6);
                    String[] command = { command_1 };

                 //Act
                    cp.CheckParams_trian(command, ref arf1, ref arf2, ref arf3, ref arf4, ref arf5, ref arf6);
                }

                //Assert
                catch (Exception e)
                {
                    ex = e;
                }
                Assert.IsNull(ex, "Erro");
                

            }// End Test_CheckParams_trians

        }

        [TestMethod]
        public void Test_CheckParams()
        {
            {
                Exception ex = null;
                try
                {
                    CommandParser cp = new CommandParser();

                 //Arrange
                    String command_1 = "drawto";
                    String b1 = "11";
                    String b2 = "33";
                    String b3 = "21";
                    String b4 = "31";
                    int arf1 = int.Parse(b1);
                    int arf2 = int.Parse(b2);
                    int x = int.Parse(b3);
                    int y = int.Parse(b4);

                 //Act
                    String[] command = { command_1 };
                    cp.CheckParams(command, ref arf1,ref arf2,ref x,ref y);
                }

                 //Assert
                catch (Exception e)
                {
                    ex = e;
                }
                Assert.IsNull(ex, "Erro");


            }// End Test_CheckParams

        }
    }  }

